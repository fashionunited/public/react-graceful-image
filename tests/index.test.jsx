import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GracefulImage from '../src';

Enzyme.configure({ adapter: new Adapter() });

let imageOnload = null;

// Mocking Image.prototype to call the onload
const mockImage = (event) => {
  Object.defineProperty(Image.prototype, event, {
    get() {
      return this.mockOnload;
    },
    set(fn) {
      imageOnload = fn;
      this.mockOnload = fn;
    },
    // Properties defined through Object.defineProperty() are, by default,
    //  non-configurable. To allow them to be redefined, or reconfigured,
    //  they have to be defined with this attribute set to true.
    configurable: true,
  });
};

const windowIntersectionObserver = window.IntersectionObserver;
const windowSetTimeout = window.setTimeout;
const observe = jest.fn();
const disconnect = jest.fn();
const accumulations = ['multiply', 'add', 'noop'];
const retryDelays = {
  multiply: 10000,
  add: 200,
  noop: 100,
};

describe('react-graceful-image', () => {
  beforeEach(() => {
    window.IntersectionObserver = jest.fn(function mockIntersectionObserver() {
      this.observe = observe;
      this.disconnect = disconnect;
    });
    observe.mockClear();
    disconnect.mockClear();
  });
  afterEach(() => {
    window.IntersectionObserver = windowIntersectionObserver;
  });

  it('should render without error', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      width: '150',
      height: '150',
    };
    const div = document.createElement('div');
    ReactDOM.render(<GracefulImage {...props} />, div);
  });

  it('should render SVG placeholder when image has not loaded', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      width: '150',
      height: '150',
    };
    let placeholder = "data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg' width%3D'{{w}}' height%3D'{{h}}' viewBox%3D'0 0 {{w}} {{h}}'%2F%3E";
    placeholder = placeholder
      .replace(/{{w}}/g, props.width)
      .replace(/{{h}}/g, props.height);

    const shallowWrapper = shallow(<GracefulImage {...props} />);

    expect(shallowWrapper.find('img').length).toBe(1);
    expect(shallowWrapper.find('img').prop('src')).toEqual(placeholder);
    expect(shallowWrapper.find('img').prop('width')).toEqual(props.width);
    expect(shallowWrapper.find('img').prop('height')).toEqual(props.height);
  });

  it('should render image when image has loaded', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      width: '150',
      height: '150',
    };
    const shallowWrapper = shallow(<GracefulImage {...props} />);
    shallowWrapper.setState({ loaded: true });

    expect(shallowWrapper.find('img').length).toBe(1);
    expect(shallowWrapper.find('img').prop('src')).toEqual(props.src);
    expect(shallowWrapper.find('img').prop('width')).toEqual(props.width);
    expect(shallowWrapper.find('img').prop('height')).toEqual(props.height);
  });

  it('should not render anything when given noPlaceholder prop until image has loaded', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      width: '150',
      height: '150',
      noPlaceholder: true,
    };
    const shallowWrapper = shallow(<GracefulImage {...props} />);
    expect(shallowWrapper.find('img').length).toBe(0);

    shallowWrapper.setState({ loaded: true });
    expect(shallowWrapper.find('img').length).toBe(1);
  });

  it("should change placeholder's color", () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      width: '150',
      height: '150',
      placeholderColor: '#aaa',
    };
    const shallowWrapper = shallow(<GracefulImage {...props} />);

    expect(shallowWrapper.find('img').prop('style')).toHaveProperty(
      'background',
      props.placeholderColor,
    );
  });

  it("should change placeholder's and loaded image's alt value", () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      width: '150',
      height: '150',
      alt: 'hello world',
    };
    const shallowWrapper = shallow(<GracefulImage {...props} />);
    expect(shallowWrapper.find('img').prop('alt')).toEqual(props.alt);

    shallowWrapper.setState({ loaded: true });
    expect(shallowWrapper.find('img').prop('alt')).toEqual(props.alt);
  });

  it('should apply className to placeholder image and loaded image', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      width: '150',
      height: '150',
      className: 'graceful_image',
    };
    const shallowWrapper = shallow(<GracefulImage {...props} />);
    expect(shallowWrapper.find('img').prop('className')).toEqual(
      props.className,
    );

    shallowWrapper.setState({ loaded: true });
    expect(shallowWrapper.find('img').prop('className')).toEqual(
      props.className,
    );
  });

  it('should observe', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      placeholder: 'https://linasmickevicius.com/images/placeholder.png',
      width: '150',
      height: '150',
    };
    mount(<GracefulImage {...props} />);

    expect(observe).toHaveBeenCalled();
    expect(observe).toHaveBeenCalledTimes(1);

    expect(observe.mock.calls[0][0].src).toEqual('https://linasmickevicius.com/images/placeholder.png');
  });

  it('should not observe when given noLazyLoad prop', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      placeholder: 'https://linasmickevicius.com/images/placeholder.png',
      width: '150',
      height: '150',
      noLazyLoad: true,
    };
    mount(<GracefulImage {...props} />);

    expect(observe).not.toHaveBeenCalled();
  });

  it('should disconnect the observer on componentWillUnmount', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      placeholder: 'https://linasmickevicius.com/images/placeholder.png',
      width: '150',
      height: '150',
    };
    const component = mount(<GracefulImage {...props} />);
    const instance = component.instance();

    instance.componentWillUnmount();

    expect(disconnect).toHaveBeenCalled();
  });

  it('should not call the on load image event when component is unmounted', () => {
    mockImage('onload');
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      width: '150',
      height: '150',
    };

    const component = mount(<GracefulImage {...props} />);
    const instance = component.instance();
    instance.loadImage();
    instance.image.onload = jest.fn();
    imageOnload();
    expect(instance.image.onload).toHaveBeenCalledTimes(1);
    component.unmount();
    // This function triggers the onload event listener
    // placed on the Component
    imageOnload();
    expect(instance.image).toBe(undefined);
    expect(component.exists()).toBe(false);
  });

  it('should try to reload the image when there is an error', () => {
    mockImage('onerror');
    const props = {
      src: 'this image is broken',
      width: '150',
      height: '150',
    };

    const component = mount(<GracefulImage {...props} />);
    const instance = component.instance();
    instance.loadImage();
    instance.image.onerror = jest.fn();
    imageOnload();
    expect(instance.image.onerror).toHaveBeenCalledTimes(1);
    expect(component.state('retryCount')).toBe(1);
  });

  accumulations.forEach(accumulate => describe(`when using accumulate: ${accumulate}`, () => {
    beforeEach(() => {
      window.setTimeout = jest.fn((fn) => {
        fn();
      });
    });
    afterEach(() => {
      window.setTimeout = windowSetTimeout;
    });

    it('should try to reload the image', () => {
      const props = {
        src: 'this image is broken',
        width: '150',
        height: '150',
        retry: {
          accumulate,
          delay: 100,
          count: 2,
        },
      };

      const component = mount(<GracefulImage {...props} />);
      const instance = component.instance();
      instance.loadImage();
      instance.handleImageRetries(instance.image);

      expect(component.state('retryCount')).toBe(2);
      expect(component.state('retryDelay')).toBe(retryDelays[accumulate]);
    });
  }));

  it('should accept a custom placeholder', () => {
    const props = {
      src: 'https://linasmickevicius.com/images/browser.png',
      placeholder: 'https://this-is-a/custom-image.jpg',
    };
    const component = mount(<GracefulImage {...props} />);

    const placeholder = component.find('img').prop('src');
    expect(placeholder).toBe('https://this-is-a/custom-image.jpg');
  });

  it('should accept a srcSet, sizes', () => {
    const props = {
      srcSet: 'https://this-is-a/320-image.jpg 320w, https://this-is-a/480-image.jpg 480w, https://this-is-a/800-image.jpg 800w',
      sizes: '(max-width: 320px) 280px, (max-width: 480px) 440px, 800px',
    };
    const shallowWrapper = shallow(<GracefulImage {...props} />);
    shallowWrapper.setState({ loaded: true });

    const img = shallowWrapper.find('img');
    const srcSet = img.prop('srcSet');
    expect(srcSet).toBe('https://this-is-a/320-image.jpg 320w, https://this-is-a/480-image.jpg 480w, https://this-is-a/800-image.jpg 800w');
    const sizes = img.prop('sizes');
    expect(sizes).toBe('(max-width: 320px) 280px, (max-width: 480px) 440px, 800px');
  });
});
