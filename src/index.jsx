import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

const fadeIn = `
  @keyframes gracefulimage {
    0%   { opacity: 0.25; }
    50%  { opacity: 0.5; }
    100% { opacity: 1; }
  }
`;

const isServerSide = typeof window === 'undefined';

/*
  Creating a stylesheet to hold the fading animation
*/
const addAnimationStyles = () => {
  const exists = document.head.querySelectorAll('[data-gracefulimage]');

  if (!exists.length) {
    const styleElement = document.createElement('style');
    styleElement.setAttribute('data-gracefulimage', 'exists');
    document.head.appendChild(styleElement);
    styleElement.sheet.insertRule(fadeIn, styleElement.sheet.cssRules.length);
  }
};

const getStateFromProps = ({
  src, srcSet, placeholder, retry: { delay },
}) => ({
  src,
  srcSet,
  placeholder,
  loaded: false,
  retryDelay: delay,
  retryCount: 1,
});

class GracefulImage extends PureComponent {
  constructor(props) {
    super(props);

    this.state = getStateFromProps(props);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.src !== state.src
      || props.srcSet !== state.srcSet
      || props.placeholder !== state.placeholder) {
      return getStateFromProps(props, state);
    }

    return null;
  }

  componentDidMount() {
    addAnimationStyles();

    this.initialize();
  }

  componentDidUpdate(prevProps) {
    const { props } = this;

    if (props.src !== prevProps.src
      || props.srcSet !== prevProps.srcSet
      || props.placeholder !== prevProps.placeholder) {
      this.cleanUp();
      this.initialize();
    }
  }

  componentWillUnmount() {
    this.cleanUp();
  }

  /*
    Attempts to load an image src passed via props
    and utilises image events to track sccess / failure of the loading
  */
  initialize() {
    const { noLazyLoad } = this.props;

    // if user wants to lazy load
    if (!noLazyLoad) {
      this.observe();
    } else {
      this.loadImage();
    }
  }

  /*
    Clear timeout incase retry is still running
    And clear any existing event listeners
  */
  cleanUp() {
    if (this.timeout) {
      window.clearTimeout(this.timeout);
    }
    this.disconnectObserver();
    this.destroyImage();
  }

  /*
    Attempts to download an image, and tracks its success / failure
  */
  loadImage() {
    this.image = new Image();
    this.image.onload = () => {
      this.setState({ loaded: true });
      const { onLoad } = this.props;
      if (typeof onLoad === 'function') {
        onLoad();
      }
    };
    this.image.onerror = () => {
      this.handleImageRetries(this.image);
    };

    this.tryToFetchImage();
  }

  tryToFetchImage() {
    const {
      src, srcSet, sizes, crossOrigin,
    } = this.props;

    if (crossOrigin) {
      this.image.crossOrigin = crossOrigin;
    }

    if (src) {
      this.image.src = src;
    }

    if (srcSet) {
      this.image.srcset = srcSet;
    }

    if (sizes) {
      this.image.sizes = sizes;
    }
  }

  observe() {
    if (typeof IntersectionObserver === 'undefined' || !this.placeholderImage) {
      return;
    }

    this.observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          this.loadImage();
          this.disconnectObserver();
        }
      });
    });

    this.observer.observe(this.placeholderImage);
  }

  disconnectObserver() {
    if (this.observer) {
      this.observer.disconnect();
    }
  }

  /*
    Handles the actual re-attempts of loading the image
    following the default / provided retry algorithm
  */
  handleImageRetries() {
    this.setState({ loaded: false }, () => {
      const {
        state: { retryCount, retryDelay },
        props: { retry: { count, accumulate, delay } },
      } = this;

      if (retryCount <= count) {
        this.timeout = setTimeout(() => {
          // re-attempt fetching the image
          this.tryToFetchImage();

          // update count and delay
          this.setState((prevState) => {
            let updateDelay;
            if (accumulate === 'multiply') {
              updateDelay = prevState.retryDelay * delay;
            } else if (accumulate === 'add') {
              updateDelay = prevState.retryDelay + delay;
            } else if (accumulate === 'noop') {
              updateDelay = delay;
            } else {
              updateDelay = 'multiply';
            }

            return {
              retryDelay: updateDelay,
              retryCount: prevState.retryCount + 1,
            };
          });
        }, retryDelay * 1000);
      }
    });
  }

  /*
    Removes any pending image when component is unmounted
  */
  destroyImage() {
    if (!this.image) {
      return;
    }
    this.image.onload = () => {};
    this.image.onerror = () => {};
    delete this.image;
  }

  /*
    Sets the placeholder when there is no placeholder
  */
  placeholder() {
    const { placeholder } = this.props;

    return placeholder || this.renderPlaceholder();
  }

  /*
    Render the placeholder
  */
  renderPlaceholder() {
    const {
      width: propWidth, height: propHeight, style: {
        width: styleWidth, height: styleHeight,
      },
    } = this.props;

    const width = propWidth || styleWidth || '200';
    const height = propHeight || styleHeight || '150';

    return `\
data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg' width%3D'${
  width}' height%3D'${height}' viewBox%3D'0 0 ${width} ${height}'%2F%3E`;
  }

  /*
    - If image hasn't yet loaded AND user didn't want a placeholder OR
        SVG not supported then don't render anything
    - Else if image has loaded then render the image
    - Else render the placeholder
  */
  render() {
    const {
      state: { loaded },
      props: {
        noPlaceholder,
        placeholderColor,
        placeholder,
        src,
        srcSet,
        sizes,
        className,
        width,
        height,
        style: originalStyle,
        alt,
        crossOrigin,
      },
    } = this;
    if (!isServerSide && !loaded && noPlaceholder) return null;

    const showImage = (isServerSide && !placeholder) || loaded;
    const style = {
      background: placeholderColor,
    };

    return (
      <img
        crossOrigin={crossOrigin}
        src={showImage ? src : this.placeholder()}
        srcSet={showImage ? srcSet : null}
        sizes={showImage ? sizes : null}
        className={className}
        width={width}
        height={height}
        style={{
          ...style,
          ...originalStyle,
        }}
        alt={alt}
        ref={loaded ? null : (ref) => {
          this.placeholderImage = ref;
        }}
        key={`${placeholder}${src}${srcSet}`}
      />
    );
  }
}

GracefulImage.defaultProps = {
  src: null,
  srcSet: null,
  sizes: null,
  className: null,
  width: null,
  height: null,
  alt: 'Broken image placeholder',
  style: {},
  placeholder: null,
  placeholderColor: 'transparent',
  retry: {
    count: 8,
    delay: 2,
    accumulate: 'multiply',
  },
  noPlaceholder: false,
  noLazyLoad: false,
  crossOrigin: null,
  onLoad: null,
};

GracefulImage.propTypes = {
  src: PropTypes.string,
  srcSet: PropTypes.string,
  sizes: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  alt: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  style: PropTypes.object,
  placeholder: PropTypes.string,
  placeholderColor: PropTypes.string,
  retry: PropTypes.shape({
    count: PropTypes.number,
    delay: PropTypes.number,
    accumulate: PropTypes.string,
  }),
  noPlaceholder: PropTypes.bool,
  noLazyLoad: PropTypes.bool,
  crossOrigin: PropTypes.string,
  onLoad: PropTypes.func,
};

export default GracefulImage;
